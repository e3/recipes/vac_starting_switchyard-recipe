# vac_starting_switchyard conda recipe

Home: "https://gitlab.esss.lu.se/epics-modules/vac_starting_switchyard"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS vac_starting_switchyard module
